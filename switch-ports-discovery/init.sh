function install_if_not {
  if yum list installed "$@" >/dev/null 2>&1; then
    true
  else
    echo Installing $1
    yum install -y $1
  fi
}
install_if_not net-snmp-utils
install_if_not fping
echo Filling ARP table
fping -r 0 -q -s -g $(ip addr | grep 'inet ' | grep -v '127.0.0.1' | awk '{print $2}') 
