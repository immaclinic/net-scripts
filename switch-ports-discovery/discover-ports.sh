echo "MAC;VLAN;PORT;HOST" > switch-$1.csv
snmpwalk -v2c -c public $1 1.3.6.1.2.1.17.7.1.2.2.1.2 | sed "s/\./ /g" | sed "s/SNMPv2-SMI::mib-2 17 7 1 2 2 1 2 //g" | sed "s/ = INTEGER://g"  | awk '{printf "%02X:%02X:%02X:%02X:%02X:%02X %s %s \n",$2,$3,$4,$5,$6,$7,$1,$8}' | awk '{printf("%s;%s;", $1, $2);if ($3 > 48) printf "GE"$3 - 48; else printf $3;printf ";"; system("./get-host.sh "$1); printf("\n")}'  >> switch-$1.csv

